# FROM arm32v6/python:2-alpine
FROM python:2-slim

RUN adduser --home /code --disabled-password --gecos "" user

COPY . /code

RUN pip install -r /code/requirements.txt \
 && cp /code/settings.py.docker /code/settings.py

WORKDIR /code
USER user

RUN touch /code/feeds.env

ENTRYPOINT ["python", "feedfetcher.py"]
